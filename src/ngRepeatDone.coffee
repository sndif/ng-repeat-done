angular
  .module 'ngRepeatDone', []
  .directive 'ngRepeatDone', ->
    {
      restrict : 'A'
      link : ( scope, $elem, attrs ) ->
        if scope.$last
          scope.$eval attrs.ngRepeatDone
    }

